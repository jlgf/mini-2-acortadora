from django.urls import path
from . import views

urlpatterns = [
	path('', views.general),
	path('<str:resource>', views.rsc),
]