from django.http import HttpResponse, Http404
from .models import Shortened
from django.template import loader # Importar el cargador de templates de django

from django.views.decorators.csrf import csrf_exempt # Para saltar el mecanismo de seguridad del 403

# Create your views here.
def general(request):
    content_list = Shortened.objects.all()

    if request.method == "GET":
        template = loader.get_template("acorta/general_get.html")
        context = {
            'short_list': content_list
        }

    elif request.method == "POST":
        url = request.POST["url"]
        shorten = request.POST["short"]

        print("POST received")

        # Check if the short is empty

        if shorten == "":
            print("No short, use random")
            try:
                print("New random")
                rnum = Shortened.objects.get(url="Rand")
                rnum.short = str(int(rnum.short) + 1)

            except Shortened.DoesNotExist:
                rnum = Shortened(url="Rand", short="1")

            shorten = rnum.short
            print("Random number to save")
            rnum.save()

        # Check if the short already exist in the database
        try:
            print("Try to get in database")
            cont = Shortened.objects.get(url=url)
            cont.short = shorten

        except Shortened.DoesNotExist:
            print("New element")
            cont = Shortened(url=url, short=shorten)

        print("Element to save")
        cont.save()

        content_list = Shortened.objects.all()

        template = loader.get_template("acorta/general_get.html")
        context = {
            'short_list': content_list
        }

    else:
        raise Http404("Method not allowed")

    return HttpResponse(template.render(context, request))


@csrf_exempt
def rsc(request, resource):

    if request.method == "GET":
        try:
            content = Shortened.objects.get(short = resource)

        except Shortened.DoesNotExist:
            raise Http404("Resource not found")

        template = loader.get_template("acorta/get_rsc.html")
        context = {
            'element': content
        }

        return HttpResponse(template.render(context, request))

    else:
        raise Http404("Method not allowed")