from django.db import models

# Create your models here.

class Shortened(models.Model):
    url = models.CharField(max_length=500)
    short = models.CharField(max_length=10)


